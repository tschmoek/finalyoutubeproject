﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FinalYotubeProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
        
            var client = new RestClient("https://ussouthcentral.services.azureml.net/workspaces/e2829c7960194e7b94b15a62f230377f/services/f1c701041ab8469396e5638337360733/execute?api-version=2.0&format=swagger");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "06dd6b5e-41ef-1baa-e89b-13f298b1acf9");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Authorization", "Bearer cDqFTmKrfI/sG5Eo1XOyvGkUUzmYBXCzg6CqecqmpLPc58b82daW/3vqb50G6uTQA6tdt13nc9UvhTiX/PzmGQ==");
            request.AddHeader("Content-Type", "application/json");
            //var json = JsonConvert.SerializeObject(root);
            //request.AddJsonBody(root); // HERE
            //request.AddParameter("text/json", json, ParameterType.RequestBody);
            string title = "BLUE PLANET II THE PREQUEL";
            string category_name = "Pets & Animals";
            string views = "1075482";
            request.AddParameter("undefined", 
                "{\r\n\"Inputs\": {\r\n\"input1\":\r\n[\r\n{\r\n\"title\": "+ "\"" + title + "\"" + ",\r\n\"category_name\": " + "\"" + category_name + "\"" + ",\r\n\"views\": " + "\"" + views + "\"" + "\r\n}\r\n]\r\n}\r\n}\r\n", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            RootObject results = new JavaScriptSerializer().Deserialize<RootObject>(response.Content);
            string content = response.Content;
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public class Output1
        {
            public string ScoredLabelMean { get; set; }
        }

        public class Results
        {
            public List<Output1> output1 { get; set; }
        }

        public class RootObject
        {
            public Results Results { get; set; }
        }
    }
}